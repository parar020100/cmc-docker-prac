from flask import Flask, request
from urllib.parse import urlparse
import psycopg2
import os

app = Flask(__name__)

database_env = os.getenv("DATABASE_URI")

result = urlparse(database_env)

conn = psycopg2.connect(
    database = result.path[1:],
    user = result.username,
    password = result.password,
    host = result.hostname,
    port = result.port
)

cur = conn.cursor()

cur.execute('''
    CREATE TABLE IF NOT EXISTS get_queries (
        id SERIAL PRIMARY KEY,
        ip VARCHAR(15)
    )
''')
conn.commit()

@app.route('/', methods=['GET'])
def handle_get_request():
    if request.method == 'GET':
        client_ip = request.remote_addr

        cur.execute("INSERT INTO get_queries(ip) VALUES (%s)", (client_ip,))
        conn.commit()

        cur.execute("SELECT count(id), ip FROM get_queries GROUP BY ip;")
        rows = cur.fetchall()
        result = [(row[1] + ': ' + str(row[0])) for row in rows]
        return ';\t'.join(result)
    else:
        return 'Unsupported request'

if __name__ == '__main__':
    app.run(debug = True, port=5000, host="0.0.0.0")