#! /bin/bash

docker context use default

minikube delete && minikube start

cd query_counter
docker build -t frontend/query_counter:task_version .
minikube image load frontend/query_counter:task_version && minikube image ls
cd ..

kubectl create ns kubetask && kubectl get ns
kubectl apply -f database.yaml -n kubetask
sleep 30
kubectl get all -n kubetask

kubectl apply -f frontend.yaml -n kubetask
sleep 30
kubectl get all -n kubetask

kubectl port-forward -n kubetask pod/frontend-pod --address 0.0.0.0 5000:5000 &

curl localhost:5000
curl localhost:5000
curl localhost:5000
curl localhost:5000